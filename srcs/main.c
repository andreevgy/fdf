/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: marvin <marvin@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/26 15:12:02 by marvin            #+#    #+#             */
/*   Updated: 2019/01/29 14:29:31 by marvin           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		draw_interface(t_map *map)
{
	mlx_string_put(map->mlx_ptr, map->win_ptr, 10, 30, W, "Controls:");
	mlx_string_put(map->mlx_ptr, map->win_ptr, 30, 50,
			W, "Scroll up/down - zoom");
	mlx_string_put(map->mlx_ptr, map->win_ptr, 30, 70, W, "Arrows - movement");
	mlx_string_put(map->mlx_ptr, map->win_ptr, 30, 90,
			W, "P - change projection");
	mlx_string_put(map->mlx_ptr, map->win_ptr, 30, 110,
		W, "B - change background");
	mlx_string_put(map->mlx_ptr, map->win_ptr, 30, 130,
			W, "Rotating on numpad:");
	mlx_string_put(map->mlx_ptr, map->win_ptr, 50, 150, W, "4/1 - x axis");
	mlx_string_put(map->mlx_ptr, map->win_ptr, 50, 170, W, "5/2 - y axis");
	mlx_string_put(map->mlx_ptr, map->win_ptr, 50, 190, W, "6/3 - z axis");
	mlx_string_put(map->mlx_ptr, map->win_ptr, 10, 220, W, "Info: ");
	mlx_string_put(map->mlx_ptr, map->win_ptr, 30, 240, W, "Zoom: ");
	mlx_string_put(map->mlx_ptr, map->win_ptr, 80, 240, W, ft_itoa(map->zoom));
	mlx_string_put(map->mlx_ptr, map->win_ptr, 30, 260, W, "Projection type: ");
	if (map->iso)
		mlx_string_put(map->mlx_ptr, map->win_ptr, 190, 260, W, "isometric");
	else
		mlx_string_put(map->mlx_ptr, map->win_ptr, 190, 260, W, "parallel");
	return (0);
}

int		draw_on_update(t_map *map)
{
	if (map->updated)
	{
		draw_all_on_image(map);
		mlx_put_image_to_window(map->mlx_ptr, map->win_ptr, map->image, 0, 0);
		mlx_string_put(map->mlx_ptr, map->win_ptr, 10, 10, W,
				"I - show interface");
		if (map->show_interface)
			draw_interface(map);
		map->updated = 0;
	}
	return (0);
}

void	hooks_buttons(t_map *map)
{
	draw_interface(map);
	draw_all_on_image(map);
	mlx_loop_hook(map->mlx_ptr, &draw_on_update, map);
	mlx_hook(map->win_ptr, 3, 0, &key_release, map);
	mlx_hook(map->win_ptr, 2, 0, &key_press, map);
	mlx_hook(map->win_ptr, 17, 0, &close_window, NULL);
	mlx_hook(map->win_ptr, 4, 0, &mouse_release, map);
	mlx_loop(map->mlx_ptr);
}

int		main(int argc, char **argv)
{
	void	*mlx_ptr;
	void	*win_ptr;
	t_map	*map;

	if (argc != 2)
	{
		ft_putendl("usage: ./fdf map_name");
		return (0);
	}
	mlx_ptr = NULL;
	mlx_ptr = mlx_init();
	win_ptr = mlx_new_window(mlx_ptr, WINDOW_WIDTH, WINDOW_HEIGHT, "test");
	map = read_file(argv[1], mlx_ptr, win_ptr);
	if (!map)
	{
		ft_putstr("Invalid map\n");
		exit(1);
	}
	map->image = create_new_image(map);
	hooks_buttons(map);
}
