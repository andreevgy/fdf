/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: marvin <marvin@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/08 17:49:38 by marvin            #+#    #+#             */
/*   Updated: 2019/01/25 13:20:30 by marvin           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	move_map(int keycode, t_map *map)
{
	if (keycode == 123)
		map->movement->x -= 40;
	if (keycode == 124)
		map->movement->x += 40;
	if (keycode == 126)
		map->movement->y -= 40;
	if (keycode == 125)
		map->movement->y += 40;
}

void	rotate_map(int keycode, t_map *map)
{
	if (keycode == 88)
		map->z_angle += 0.1;
	if (keycode == 85)
		map->z_angle -= 0.1;
	if (keycode == 86)
		map->x_angle += 0.1;
	if (keycode == 83)
		map->x_angle -= 0.1;
	if (keycode == 87)
		map->y_angle += 0.1;
	if (keycode == 84)
		map->y_angle -= 0.1;
}

t_map	*create_map(int width, int height, void *mlx_ptr, void *win_ptr)
{
	t_map *map;

	NULL_CHECK((map = (t_map *)ft_memalloc(sizeof(t_map))));
	map->width = width;
	map->height = height;
	NULL_CHECK((map->map = (int **)ft_memalloc(sizeof(int *) * height)));
	map->zoom = 15;
	map->z_angle = 0;
	map->x_angle = 0;
	map->y_angle = 0;
	map->size_line = 0;
	map->background = 1;
	map->iso = 1;
	map->data_addr = NULL;
	map->updated = 1;
	NULL_CHECK((map->colors = (int **)ft_memalloc(sizeof(int *) * height)));
	map->mlx_ptr = mlx_ptr;
	map->win_ptr = win_ptr;
	map->show_interface = 1;
	map->movement = create_dot(WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2, 0xFFFFFF);
	NULL_CHECK((map->dots = (t_dot **)ft_memalloc(sizeof(t_dot *)
					* width * height)));
	return (map);
}
