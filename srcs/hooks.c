/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hooks.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: marvin <marvin@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/09 15:10:43 by marvin            #+#    #+#             */
/*   Updated: 2019/01/25 13:15:28 by marvin           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int	key_press(int keycode, t_map *map)
{
	map->updated = 1;
	if (keycode >= 123 && keycode <= 126)
		move_map(keycode, map);
	if (keycode >= 83 && keycode <= 88)
		rotate_map(keycode, map);
	if (keycode == 34)
		map->show_interface = !(map->show_interface);
	if (keycode == 35)
		map->iso = !(map->iso);
	if (keycode == 11)
		map->background = !(map->background);
	return (0);
}

int	key_release(int keycode, void *param)
{
	if (keycode == 53)
		close_window(param);
	return (0);
}

int	mouse_release(int button, int x, int y, t_map *map)
{
	(void)x;
	(void)y;
	map->updated = 1;
	if (button == 4)
		map->zoom++;
	if (button == 5 && map->zoom > 0)
		map->zoom--;
	return (0);
}

int	close_window(void *param)
{
	(void)param;
	exit(0);
	return (0);
}
